import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * Created by david on 7/16/16.
 */
public class TestTextBlockParser {

    private LetterCounter mockCounter;
    private LetterMapSorter mockSorter;
    private TextBlockParser parser;
    private String inputString;
    private Map<String, Integer> inputStringMap;

    @Before
    public void setUp() throws Exception {
        mockCounter = mock(LetterCounter.class);
        mockSorter = mock(LetterMapSorter.class);
        inputString = "Doesn'tMatter";
        inputStringMap = new HashMap<>();
    }

    @Test
    public void itShouldCallLetterUtilities() throws Exception {
        when(mockCounter.countItems(any())).thenReturn(inputStringMap);
        when(mockSorter.sortMap(any())).thenReturn("mockStringValue");
        parser = new TextBlockParser(mockCounter, mockSorter);

        parser.getSecretWord(inputString);
        verify(mockCounter).countItems(inputString);
        verify(mockSorter).sortMap(inputStringMap);
    }

    @Test
    public void itShouldReturnTheSecretWord() throws Exception {
        when(mockCounter.countItems(any())).thenReturn(inputStringMap);
        when(mockSorter.sortMap(any())).thenReturn("lucio_zsrnmp");
        parser = new TextBlockParser(mockCounter, mockSorter);

        String result = parser.getSecretWord(inputString);

        assertEquals("lucio", result);
    }
}

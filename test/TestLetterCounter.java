import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by david on 7/16/16.
 */
public class TestLetterCounter {

    private LetterCounter counter;

    @Before
    public void setUp() throws Exception {
        counter = new LetterCounter();
    }

    @Test
    public void itShouldCountLetters() {
        String input = "cccbba";
        Map<String, Integer> result = counter.countItems(input);
        assertEquals((Integer)1, result.get("a"));
        assertEquals((Integer)2, result.get("b"));
        assertEquals((Integer)3, result.get("c"));
    }

    @Test
    public void itShouldCountUnderscores() {
        String input = "aa__bb__ccccc";
        Map<String, Integer> result = counter.countItems(input);
        assertEquals((Integer)4, result.get("_"));
        assertEquals((Integer)5, result.get("c"));
        assertEquals((Integer)2, result.get("b"));
        assertEquals((Integer)2, result.get("a"));
    }
}

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created by david on 7/16/16.
 */
public class TestLetterSorter {

    private LetterMapSorter sorter;

    @Before
    public void setUp() throws Exception {
        sorter = new LetterMapSorter();

    }

    @Test
    public void itShouldSortALetterCountMap() throws Exception {
        Map<String, Integer> input = new HashMap<>();
        input.put("a", 1);
        input.put("b", 4);
        input.put("_", 3);
        input.put("c", 0);

        String result = sorter.sortMap(input);
        assertEquals("b_ac", result);
    }
}

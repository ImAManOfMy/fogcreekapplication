import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by david on 7/16/16.
 */
public class LetterCounter {

    static String letters = "abcdefghijklmnopqrstuvwxyz_";

    public Map<String, Integer> countItems(String input) {
        Map<String, Integer> result = new HashMap<>();

        for (int i = 0; i < input.length(); i++) {
            String letter = Character.toString(input.charAt(i));
            Integer count = result.getOrDefault(letter, 0);
            result.put(letter, ++count);
        }

        return result;
    }
}

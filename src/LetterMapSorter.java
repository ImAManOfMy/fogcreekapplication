import java.util.*;

import static java.util.Map.Entry;

/**
 * Created by david on 7/16/16.
 */
public class LetterMapSorter {
    public String sortMap(Map<String, Integer> input) {
        List<Entry> entryList = new ArrayList<>(input.entrySet());

        Collections.sort(entryList, (entry1, entry2) -> {
            Integer value1 = ((Entry<String,Integer>)entry1).getValue();
            Integer value2 = ((Entry<String,Integer>)entry2).getValue();
            return value2.compareTo(value1);
        });

        StringBuilder result = new StringBuilder();

        for (Entry entry : entryList) {
            result.append(entry.getKey());
        }

        return result.toString();
    }
}
